<?php

namespace ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ApiBundle
 * @package ApiBundle
 * @author eshigaev
 */
class ApiBundle extends Bundle
{
}
