<?php

namespace ApiBundle\Controller;

use AppBundle\Form\CityType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("City", pluralize=false)
 */
class CityRestController extends FOSRestController
{

    protected
        $entity = 'AppBundle:City';

    /**
     * Find
     * @param Request $request
     * @return array
     */
    public function cgetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository($this->entity)->findAll();

        return [
            'entities' => $entities,
        ];
    }

    /**
     * Find One
     * @param $id
     * @return mixed
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($this->entity)->find($id);

        if (!$entity) throw $this->createNotFoundException('Unable to find entity');

        return $entity;
    }

    /**
     * Create
     * @param Request $request
     * @return array|\Symfony\Component\Form\Form
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(CityType::class, null, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) return $form;

        $em = $this->getDoctrine()->getManager();

        $entity = $form->getData();

        $em->persist($entity);

        $em->flush();

        return [
            'entity' => $entity,
        ];
    }

    /**
     * Update
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\Form\Form
     */
    public function putAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($this->entity)->find($id);

        if (!$entity) throw $this->createNotFoundException('Unable to find entity');

        $form = $this->createForm(CityType::class, $entity, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) return $form;

        $em->flush();

        return [
            'entity' => $entity,
        ];
    }

}