<?php

namespace ApiBundle\Controller;

use AppBundle\Service\GeoService;
use Buzz\Exception\InvalidArgumentException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @RouteResource("Geo", pluralize=false)
 */
class GeoRestController extends FOSRestController
{
    /**
     * address: string
     * lat: latitude
     * long: longitude
     * radius: meters
     *
     * @param Request $request
     * @return array
     */
    public function cgetRadiusAction(Request $request)
    {
        $address = $request->query->get('address');
        $latitude = (float)$request->query->get('lat');
        $longitude = (float)$request->query->get('long');
        $radius = (float)$request->query->get('radius');

        $geoService = $this->container->get('geo');

        $coords = $geoService->coordsByAddress($address);
        $distance = $geoService->distanceBetween($coords[ 0 ], $coords[ 1 ], $latitude, $longitude) * 1000;

        try
        {
            if (!$latitude || !$longitude || !$radius) throw new InvalidArgumentException();


        }
        catch (\Exception $e)
        {
            return [
                'result' => false
            ];
        }

        return [
            'result' => $distance <= $radius,
            'distance' => $distance,
            'coords' => $coords
        ];
    }



}