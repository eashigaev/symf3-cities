<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class GeoService
 * @package AppBundle\Services
 * @author eshigaev
 */
class GeoService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function coordsByAddress($address)
    {
        $buzz = $this->container->get('buzz');
        $result = $buzz->get('https://geocode-maps.yandex.ru/1.x/?format=json&results=1&geocode=' . $address);

        $response = json_decode($result->getContent(), true)[ 'response' ];
        $coords = \explode(' ', $response[ 'GeoObjectCollection' ][ 'featureMember' ][ 0 ][ 'GeoObject' ][ 'Point' ][ 'pos' ]);

        return array_reverse($coords);
    }

    public function distanceBetween($latitude1, $longitude1, $latitude2, $longitude2)
    {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }
}