<?php

namespace CrudBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CrudBundle
 * @package CrudBundle
 * @author eshigaev
 */
class CrudBundle extends Bundle
{
}
