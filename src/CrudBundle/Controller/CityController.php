<?php

namespace CrudBundle\Controller;

use AppBundle\Entity\City;
use AppBundle\Form\CityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CityController extends Controller
{
    protected
        $entity = 'AppBundle:City',
        $views = 'CrudBundle:City:';

    /**
     * @Route("/", name="city.list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository($this->entity)->findAll();

        return $this->render($this->views . 'list.html.twig', [
            'entities' => $entities
        ]);
    }

    /**
     * @Route("/create", name="city.create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CityType::class)
            ->add('save', SubmitType::class, ['label' => 'Create', 'attr' => [
                'class' => 'btn btn-default'
            ]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var $city City
             */
            $city = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($city);
            $em->flush();

            // for now
            return $this->redirectToRoute('city.edit', [
                'city' => $city->getId(),
            ]);

        }

        return $this->render($this->views . 'create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request  $request
     * @param City $city
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/edit/{city}", name="city.edit")
     */
    public function editAction(Request $request, City $city)
    {
        $form = $this->createForm(CityType::class, $city)
            ->add('save', SubmitType::class, ['label' => 'Update', 'attr' => [
                'class' => 'btn btn-default'
            ]]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            // for now
            return $this->redirectToRoute('city.list');

        }

        return $this->render($this->views . 'edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request  $request
     * @param City $city
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/delete/{city}", name="city.delete")
     */
    public function deleteAction(Request $request, City $city)
    {
        if ($city !== null)
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($city);
            $em->flush();
        }

        return $this->redirectToRoute('city.list');
    }
}